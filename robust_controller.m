close all
clear all
tic

%% Load first level optimization conditions
load('open_loop.mat'); %% References form the first level optimization
load('hessian.mat'); %% Approximation of the Hessian of the first level objective function
tic
global Km Kmp gobal ev Ay At Ax x0 Bt Btx Bty Dt b1 b2 nu N uref xref bb1 bb2 Ai

%% Reference values from first level optimization
xref=[Eva(2:end);Ear(2:end)]';
xrefc=[Eva;Ear]';
xref1=[Evl1(2:end);Evl1(2:end)]';
xref2=[Evl2(2:end);Evl2(2:end)]';
xref3=[Evl3(2:end);Evl3(2:end)]';
uref = [Pc,Pd];
uref1 = [Prd,Pru];

%%%% load uncertainty characterization of car-sharing demand and energy consumption data%%%%%%%%%%%%%%
%% Mean values

Edpf=load('inputs/mu_edp.mat'); %% Energy consuption in travels
Ntdf=load('inputs/mu_ntd.mat');%% Car-sharing demand (number of vehicles traveling among the stations)
Navf=load('inputs/mu_nav.mat'); %% Number of vehicles parked at charging stations
gdaf=load('inputs/mu_gda.mat'); %% Ratio departing/available vehicles at charging stations

Edp = Edpf.mu_edp;
gda = gdaf.mu_gda;

%% Variance
Edpfs=load('inputs/std_edp.mat');%% Energy consuption in travels
Ntdfs=load('inputs/std_ntd.mat');%% Car-sharing demand (number of vehicles traveling among the stations)
Navfs=load('inputs/std_nav.mat');%% Number of vehicles parked at charging stations
gdafs=load('inputs/std_gda.mat');%% Ratio departing/available vehicles at charging stations

Edpv = Edpfs.std_edp;
Ntdv = Ntdfs.std_ntd;
Navv = Navfs.std_nav;
gdav = gdafs.std_gda;


%% Confidence level an relation with variance
eps=0.2;
eps_i=eps;
clev = sqrt((1-eps_i)/eps_i);

%% Upper and lower bound for energy expenditure in travels
Edpvu = (clev/2).*gch'.*Edpv(:,2);
Edpvl = (clev/2).*gch'.*Edpv(:,3);


%% Upper and lower bound for Ratio departing/available vehicles at charging stations
for kk=1:N
    if kk>=2
    gmu(kk) = (clev/2).*(gch(kk)/gch(kk-1)).*gdav(kk,2);
    gml(kk) = (clev/2).*(gch(kk)/gch(kk-1)).*gdav(kk,3);
    else
    gmu(kk) = (clev/2).*(gch(kk)).*gdav(kk,2);
    gml(kk) = (clev/2).*(gch(kk)).*gdav(kk,3);
    end
end

for kk=1:N
    if gda(kk)+ gmu(kk) > 1
        gmu(kk)=1-gda(kk);
    end
end

for kk=1:N
    if gda(kk) + gml(kk) < 0
        gml(kk)= max(gda(kk) + gml(kk),0);
    end
end

%% Upper and lower bound for vehicles available at charging stations
Navvu = round((clev/2).*gch'.*Navv(1:end-1,2));
Navvl = round((clev/2).*gch'.*Navv(1:end-1,3));


for kk=1:N
    if Nav(kk)+ Navvu(kk) > ev.N_fleet
        Navvu(kk)=ev.N_fleet-Nav(kk);
    end
end

for kk=1:N
    if Nav(kk)+ Navvl(kk) < 0
        Navvl(kk)= min(-Nav(kk),0);
    end
end

Ntdu = round((clev/2).*gch'.*Ntdv(:,2));
Ntdl = -round((clev/2).*gch'.*Ntdv(:,2));
Ntdx = Ntdf.mu_ntd.*gch';

%% Upper and lower bound for vehicles arriving to charging stations
Naru(1)=0;
Narl(1)=0;

for kk=1:N
   Naru(kk+1)=Ntdx(kk)+Ntdu(kk);
   Narl(kk+1)=Ntdx(kk)+Ntdl(kk);
end


for kk=1:N
    if Narl(kk) < 0
        Narl(kk)= 0;
    end
end


%% Dynamic ssystem description model 
Nt=2;
B = [ev.n_c*(1/(Nt-1)) -(1/ev.n_d)*(1/(Nt-1)); 0 0]; %% matrix of incidence of inputs over the states
Bd = [0;1]; %% matrix of incidence of uncertainties over the states


%% %% matrix of incidence of states over the states under mean conditions
As={};
for kk = 1:24
As{kk} = [1-(Ntd1(kk)/Nav(kk)) 1;(Ntd1(kk)/Nav(kk)) 0];
end;

An = {};
%% matrix indicating the states are completely observable
C = eye(2);
Edps=Edp;
Ci = [1 0;0 1];

%% discretization parameters
dt = 1; %% time step 1h
N = 24; %%% Total calculation time

%% %% matrix of incidence of states over the states under perturbed conditions
Ai={};
for kk = 1:N
Ai{kk} = [1-(Ntd1(kk)/Nav(kk)) 1;(Ntd1(kk)/Nav(kk)) 0];
end

%% %% Mean value of additive disturbances
dp=[];
for ii=1:N
    dp=[dp,-Edps(ii)];
end


%% Number of state variables
nx = 2;
%% number of vertex in state multiplicative disturbance
nvx=2;

%% %% matrix of incidence of states over the states only perturbed part
Aip={};
for kk = 1:N
     Aip{kk,1} = [-gmu(kk) 0;gmu(kk) 0];
     Aip{kk,2} = [-gml(kk) 0;gml(kk) 0];
end

Aipp={};
for kk = 1:N
     Aipp{kk,1} = [-gmu(kk);gmu(kk)]*[1 0];
     Aipp{kk,2} = [-gml(kk);gml(kk)]*[-1 0];
end



%% Matrices resulting of develop in time, the dynamic model
An={};
for ii=1:N
    An{ii} = [Ai{ii} zeros(2,2);
        C*Ai{ii} eye(2)];
end

Anp={};
for ii=1:N
    for jj=1:2
        Anp{ii,jj} = [Aip{ii,jj} zeros(2,2);
            C*Aip{ii,jj} zeros(2,2)];
    end
end

Bn={};
for ii=1:N
Bn{ii} = [B;C*B];
end

Bd=[0;-1];

Dn={};
for ii=1:N
Dn{ii} = [Bd;C*Bd];
end


%% number of expanded states with the integral of error
nx=4;
nu = 2;

%% Robust gain Riccati equation SDP approach
%% R and Q tuning matrices for LQR
R= 0.001.*eye(2);
Q=1000.*[1 0 0 0;0 1 0 0;0 0 0.01 0;0 0 0 9];

x0 =[Eva(1);Ear(1)]';

cvx_begin sdp
    variable al(1)
    variable S(nx,nx,N+1) 
    variable Y(nu,nx,N)
minimize(al)
subject to
al>0;


[1 zeros(1,2) zeros(1,2);
 [zeros(2,1);zeros(1,2)'] S(:,:,N+1)] >= 0;
S(:,:,N+1)==al*inv(Q);
for kk=N:-1:1
    for jj=1:2
    S(:,:,kk)== hermitian_semidefinite( nx );
   [S(:,:,kk) (An{kk}+Anp{kk,jj}*S(:,:,kk)+Bn{kk}*Y(:,:,kk))' S(:,:,kk)*Q^(1/2) Y(:,:,kk)'*R^(1/2); 
       (An{kk}+Anp{kk,jj}*S(:,:,kk)+Bn{kk}*Y(:,:,kk)) S(:,:,kk+1) zeros(nx,nx) zeros(nx,nu);
      Q^(1/2)* S(:,:,kk) zeros(nx,nx) al*eye(nx) zeros(nx,nu);
      R^(1/2)*Y(:,:,kk) zeros(nu,nx) zeros(nu,nx) al*eye(nu)] >=0;  
    end
end
S(:,:,N+1)

cvx_end

Km= {};
Pf={};
for kk=1:N

Km{kk}= Y(:,:,kk)*inv(S(:,:,kk));
Kmp{kk}=Km{kk}(1:2,:);
Pf{kk} = al*inv(S(:,:,kk));
end

Pff = al*inv(S(:,:,N+1));



for kk = 1:N
    for jj=1:2
     Aipf{kk,jj} = (An{kk}+Anp{kk,jj})+ Bn{kk}*Km{kk};
    end
end

Bd = [0;-1];


%% Contraints of the problem
bb1 = {};
bb2 = {};
b1 = {};
b2 = {};

for kk=1:N
 bb1{kk} = [Nav(kk)*ev.Emax;Nara(kk)*ev.Emax];
 bb2{kk} = [Nav(kk)*ev.Emin;0];
 b1{kk} = [Nav(kk)*ev.Pmaxr;Nara(kk)*ev.Pmaxr];
 b2{kk} = [0;0];
end


%% Min-max value of additive disturbances
Wk=[(Edpvu)';
    (Edpvl)'];


%% Apprach for variables e, phi, z
eiu={};
eil={};
eiu{1,1} = [0;0;0;0];
eil{1,1} = [0;0;0;0];
eiu{1,2} = [0;0;0;0];
eil{1,2} = [0;0;0;0];

for kk=2:N+1
    for jj=1:2
    eiu{kk,jj} = Aipf{kk-1,jj}*eiu{kk-1,jj}+Dn{kk-1}*Wk(1,kk-1);
    eil{kk,jj} = Aipf{kk-1,jj}*eil{kk-1,jj}+Dn{kk-1}*Wk(2,kk-1);
    end
end




x0=[0;0;0;0];

dxmaxf = {};
dxminf = {};
dumax = {};
dumin = {};
bf={};
Navp=round(Nav);
for kk=1:N
dxmin1{kk} = max([[(Navp(kk))*ev.Emin;(Nara(kk))*ev.Emin],...
    [(Navp(kk))*ev.Emin;(Nara(kk))*ev.Emin]-ev.n_c.*uref1(kk,1),...
    [(Navp(kk))*ev.Emin;(Nara(kk))*ev.Emin]+(1/ev.n_d).*uref1(kk,2)],[],2);
dxmax1{kk} = min([[round(Navp(kk))*ev.Emax;round(Nara(kk))*ev.Emax],...
    [(Navp(kk))*ev.Emax;(Nara(kk))*ev.Emax]-ev.n_c.*uref1(kk,1),...
    [(Navp(kk))*ev.Emax;(Nara(kk))*ev.Emax]+(1/ev.n_d).*uref1(kk,2)],[],2);
dumax1{kk} = [Nav(kk)*ev.Pmaxr;Nav(kk)*ev.Pmaxr];
dumin1{kk}=  [0;0];
bf{kk} = [dxmax1{kk}; -dxmin1{kk}; dumax1{kk}; -dumin1{kk}];

end


%%% perturbated limits
dxmaxfp = {};
dxminfp = {};
dumaxp = {};
duminp = {};
for kk=1:N
    xrefn{kk,2}=Aipp{kk,2}*xrefc(kk,:)'+xrefc(kk+1,:)';
    xrefn{kk,1}=Aipp{kk,1}*xrefc(kk,:)'+xrefc(kk+1,:)';
    dxminfp{kk,2} = -max([[(Navvu(kk)+Navp(kk+1))*ev.Emin;(Narl(kk+1))*ev.Emin],...
    [(Navvu(kk)+Navp(kk+1))*ev.Emin;(Narl(kk+1))*ev.Emin]-ev.n_c.*uref1(kk,1),...
    [(Navvu(kk)+Navp(kk+1))*ev.Emin;(Narl(kk+1))*ev.Emin]+(1/ev.n_d).*uref1(kk,2)],[],2)+xrefn{kk,2};

    dxmaxfp{kk,2} = min([[(Navvu(kk)+Navp(kk+1))*ev.Emax;(Narl(kk+1))*ev.Emax],...
    [(Navvu(kk)+Navp(kk+1))*ev.Emax;(Narl(kk+1))*ev.Emax]-ev.n_c.*uref1(kk,1),...
    [(Navvu(kk)+Navp(kk+1))*ev.Emax;(Narl(kk+1))*ev.Emax]+(1/ev.n_d).*uref1(kk,2)],[],2)-xrefn{kk,2};

    dumaxp{kk,2} = [(Navvu(kk)+Navp(kk))*ev.Pmaxr;(Navvu(kk)+Navp(kk))*ev.Pmaxr]-uref(kk,:)'-uref1(kk,:)';

    duminp{kk,2} = [0;0]+uref(kk,:)';
    
    dxminfp{kk,1} = - max([[(Navvl(kk)+Navp(kk+1))*ev.Emin;(Naru(kk+1))*ev.Emin],...
    [(Navvl(kk)+Navp(kk+1))*ev.Emin;(Naru(kk+1))*ev.Emin]-ev.n_c.*uref1(kk,1),...
    [(Navvl(kk)+Navp(kk+1))*ev.Emin;(Naru(kk+1))*ev.Emin]+(1/ev.n_d).*uref1(kk,2)],[],2)+xrefn{kk,1};

    dxmaxfp{kk,1} = min([[(Navvl(kk)+Navp(kk+1))*ev.Emax;(Naru(kk+1))*ev.Emax],...
    [(Navvl(kk)+Navp(kk+1))*ev.Emax;(Naru(kk+1))*ev.Emax]-ev.n_c.*uref1(kk,1),...
    [(Navvl(kk)+Navp(kk+1))*ev.Emax;(Naru(kk+1))*ev.Emax]+(1/ev.n_d).*uref1(kk,2)],[],2)-xrefn{kk,1};

    dumaxp{kk,1} = [(Navvl(kk)+Navp(kk))*ev.Pmaxr;(Navvl(kk)+Navp(kk))*ev.Pmaxr]-uref(kk,:)'-uref1(kk,:)';

    duminp{kk,1} = [0;0]+uref(kk,:)';
   
end

bfp={};
for kk=1:N
    for jj=1:nvx
         bfp{kk,jj} = [dxmaxfp{kk,jj}; -dxminfp{kk,jj}; dumaxp{kk,jj}; -duminp{kk,jj}];
    end
end

nx1 = 4;

% %%% drifted variables for independent system
Tz= [eye(4), zeros(4,nu*N)];

Gba={};
for kk=1:N-1
    Gba{kk}=eye(2);
end
Mi= [zeros(2*N,2) [blkdiag(Gba{:});zeros(2,2*(N-1))]];

Ei= [eye(nu) zeros(2,2*(N-1))];
Eia={};
for kk=1:N
    Eia{kk}= [zeros(2,2*(kk-1)) eye(nu) zeros(2,2*(N-1-kk+1))];
end



fiia={};
fii={};
for kk=1:N
    for jj=1:nvx
    fiia{kk,jj}= [Aipf{kk,jj} Bn{kk}*Ei;
             zeros(2*N,4) Mi];
    if kk>1
            fii{kk,jj}=fiia{kk,jj}*fii{kk-1,jj};
    else
        fii{kk,jj}=fiia{kk,jj};
    end
    end
end

Db = [Dn{1};zeros(nu*N,1)];

Dbj={};
for kk=1:N
    if Edpvu(kk)>0
    Dbj{kk,1} = [Dn{1}.*(Edpvu(kk));zeros(nu*N,1)];
    Dbj{kk,2} = [Dn{1}.*(Edpvl(kk));zeros(nu*N,1)];
    else
        Dbj{kk,1} = [Dn{1};zeros(nu*N,1)];
        Dbj{kk,2} = [Dn{1};zeros(nu*N,1)];
    end
end

Dbp={};
for kk=1:N
    Dbp{kk} = [[Dn{1}.*(Edpvu(kk));zeros(nu*N,1)] [Dn{1}.*(Edpvl(kk));zeros(nu*N,1)]];
end
Eic=eye(N*nu);
%%%%%%%constraints (Fx+Gu)
Ffa={};
for kk=1:N
 Ffa{kk} = [1 0 0 0 zeros(1,nu*N);
       0 1 0 0 zeros(1,nu*N);
       -1 0 0 0 zeros(1,nu*N);
       0 -1 0 0 zeros(1,nu*N);
       Km{kk} Ei;
       - Km{kk} -Ei];
end

Ffa3={};
for kk=1:N
 Ffa3{kk} = [1 0 0 0;
       0 1 0 0;
       -1 0 0 0;
       0 -1 0 0;
       Km{kk};
       -Km{kk}];
end
   
Ffa2={};
for kk=1:N
    for jj=1:2
        Ffa2{kk,jj} =Ffa{kk}*fii{kk,jj};
    end
end

wj{1}=[1;0];
wj{2}=[0;1];

hiu={};
hil={};
for kk=1:N
    for jj=1:2
        hiu{kk,jj} = Ffa3{kk}*eiu{kk,jj};
        hil{kk,jj} = Ffa3{kk}*eil{kk,jj};
    end
end


%% Cost of dual variables 
Wcd={};
for kk=1:N
Wcd{kk}= Bn{1}'*inv(Pec{kk})*Bn{1}+Rxa{kk};
end


Wc=blkdiag(Wcd{:});

%% Optimization on dual varibles ck
nx = 4;

cvx_begin
    variable ck(nu*(N))
    variable t1(N)
    variable t2(N)
    variable t3(N)
    variable t4(N)
    variable t5(N)
    variable t6(N)
    variable t7(N)
    variable t8(N)
    variable tl1(N)
    variable tl2(N)
    variable tl3(N)
    variable tl4(N)
    variable tl5(N)
    variable tl6(N)
    variable tl7(N)
    variable tl8(N)



zkpu={};
zkpl={};
zk0=[x0;ck];
for kk = 1:N
  for jj=1:2
   if kk==1
       zkpu{kk,jj} = zk0;
       zkpl{kk,jj} = zk0;
       uku{kk,jj}=[Km{kk} Ei]*zkpu{kk,jj};
       ukl{kk,jj}=[Km{kk} Ei]*zkpl{kk,jj};

   else
   zkpu{kk,jj} = fiia{kk-1,jj}*zkpu{kk-1,jj};
   zkpl{kk,jj} = fiia{kk-1,jj}*zkpl{kk-1,jj};
   uku{kk,jj}=[Km{kk} Ei]*zkpu{kk,jj};
   ukl{kk,jj}=[Km{kk} Ei]*zkpl{kk,jj};

   end

  end
end
  tt=[t1,t2,t3,t4,t5,t6,t7,t8]';
  ttl=[tl1,tl2,tl3,tl4,tl5,tl6,tl7,tl8]';

  %% tuning of wigths for contraints and dual variables
  minimize(1*quad_form(ck,Wc)+1*(1.*norm(t1)+1.*norm(t2)+...
    1.*norm(t3)+1.*norm(t4)+5*norm(t5)+5*norm(t6)+5.*norm(t7)+...
    5.*norm(t8)+1.*norm(tl1)+1.*norm(tl2)+...
    1.*norm(tl3)+1.*norm(tl4)+9*norm(tl5)+9*norm(tl6)...
    +5.*norm(tl7)+5.*norm(tl8)))



subject to
for kk=1:N
  for jj=1:2  

    Ffa{kk}*zkpu{kk,jj} <= tt(:,kk)+bfp{kk,jj}-hiu{kk,jj};
    Ffa{kk}*zkpl{kk,jj} <= ttl(:,kk)+bfp{kk,jj}-hil{kk,jj};
      

  end
end


cvx_end


%% Solution vectors
xsoln(:,1)=zeros(4,1);
for kk=1:N
    xsoln(:,kk+1)=(An{kk}+Anp{kk,1}+Bn{kk}*Km{kk})*xsoln(:,kk)+Bn{kk}*ck(2*(kk-1)+1:2*kk)-Dn{1}*Edpvu(kk);
end



clear xks
for kk=1:N
    xks1(:,kk)=[eye(4) zeros(4,N*nu)]*zkpu{kk,1}+eiu{kk,1};
    xks2(:,kk)=[eye(4) zeros(4,N*nu)]*zkpu{kk,2}+eiu{kk,2};
    xks3(:,kk)=[eye(4) zeros(4,N*nu)]*zkpl{kk,1}+eil{kk,1};
    xks4(:,kk)=[eye(4) zeros(4,N*nu)]*zkpl{kk,2}+eil{kk,2};
    if kk==1
       uks1(:,kk)=[Km{kk} Ei]*zk0+Km{kk}*eiu{kk,1};
       uks2(:,kk)=[Km{kk} Ei]*zk0+Km{kk}*eiu{kk,2};
       uks3(:,kk)=[Km{kk} Ei]*zk0+Km{kk}*eil{kk,1};
       uks4(:,kk)=[Km{kk} Ei]*zk0+Km{kk}*eil{kk,2};
   else
   uks1(:,kk)=[Km{kk} Ei]*zkpu{kk-1,1}+Km{kk}*eiu{kk,1};
   uks2(:,kk)=[Km{kk} Ei]*zkpu{kk-1,2}+Km{kk}*eiu{kk,2};
   uks3(:,kk)=[Km{kk} Ei]*zkpl{kk-1,1}+Km{kk}*eil{kk,1};
   uks4(:,kk)=[Km{kk} Ei]*zkpl{kk-1,2}+Km{kk}*eil{kk,2};
   end
end


for kk=1:N
    if kk==1
         xksa1(:,kk)=xks1(:,kk)+[xrefc(kk,:)';xrefc(kk,:)']; 
         xksa2(:,kk)=xks2(:,kk)+[xrefc(kk,:)';xrefc(kk,:)']; 
         xksa3(:,kk)=xks3(:,kk)+[xrefc(kk,:)';xrefc(kk,:)']; 
         xksa4(:,kk)=xks4(:,kk)+[xrefc(kk,:)';xrefc(kk,:)']; 
    else
 xksa1(:,kk)=xks1(:,kk)+[xrefn{kk-1,1};xrefn{kk-1,1}]; 
 xksa2(:,kk)=xks2(:,kk)+[xrefn{kk-1,1};xrefn{kk-1,1}]; 
 xksa3(:,kk)=xks3(:,kk)+[xrefn{kk-1,2};xrefn{kk-1,2}]; 
 xksa4(:,kk)=xks4(:,kk)+[xrefn{kk-1,2};xrefn{kk-1,2}]; 
    end
end

ts=toc
figure()
plot(xksa1(1,:)','LineWidth',1.5); hold on
plot(xksa3(1,:)','LineWidth',1.5); hold on; 
plot(xref(:,1)', 'LineStyle','--','LineWidth',1.5); hold on;
plot((Navp(2:end)'+Navvl).*ev.Emax,'g--','LineWidth',1.5); hold on; 
plot((Navp(2:end)'+Navvl).*ev.Emin,'g--','LineWidth',1.5); hold on; 
grid on;
title('Energy in the aggregated battery bank');
legend('Energy in the bank for max case','Energy in the bank for min case','Ref. energy','Maximum-minimum energy capacity','location','southoutside')
xlabel('Time [Hour]');
ylabel('Energy [kWh]');
set(gca,'fontsize', 14);
axis tight

figure()
plot(xksa1(2,:)','r','LineWidth',1.5); hold on; 
plot(xksa3(2,:)','b','LineWidth',1.5); hold on; 
plot(xref(:,2)','k', 'LineStyle','--','LineWidth',1.5); hold on; grid on;
plot(Naru.*ev.Emax,'g--','LineWidth',1.5); hold on; plot(Naru.*ev.Emin,'g--','LineWidth',1.5); hold on;
legend('Energy of arriving EVs for max case','Energy of arriving EVs for min case','Ref. energy','Maximum-minimum energy capacity','location','southoutside')
title('Energy of arriving vehicles');
xlabel('Time [Hour]');
ylabel('Energy [kWh]');
set(gca,'fontsize', 14);
axis tight


figure()
plot(uks1'+uref,'LineWidth',1.5); hold on; 
plot(uks3'+uref,'LineWidth',2,'LineStyle','-.'); hold on;
plot(uref, 'LineStyle','--','LineWidth',1.5); hold on; 
plot((Navp(1:end-1)'+Navvl).*ev.Pmaxr,'g--','LineWidth',1.5); hold on; 
plot((Navp(1:end-1)'+Navvl).*0,'g--','LineWidth',1.5); hold on; 
grid on;
title('Charging/discharging power of the battery bank');
legend('Charging power for max case','Discharging power for max case','Charging power for min case','Discharging power for min case','Ref. charging power','Ref. discharging power','Maximum-minimum power capacity','location','southoutside')
xlabel('Time [Hour]');
ylabel('Power [kW]');
set(gca,'fontsize', 14);
axis tight

